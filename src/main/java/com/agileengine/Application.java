package com.agileengine;

import com.agileengine.model.Request;

public class Application {

    public static void main(String[] args) {
        Request request = Request.parseRequest(args);

        System.out.println(new SmartXMLAnalyzer().findElementPath(request));
    }
}
