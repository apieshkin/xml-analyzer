package com.agileengine.model;

public class Request {

    private String originalPath;

    private String otherPath;

    private String originalElementId = "make-everything-ok-button";

    public Request(String originalPath, String otherPath) {
        this.originalPath = originalPath;
        this.otherPath = otherPath;
    }

    public String getOriginalPath() {
        return originalPath;
    }

    public void setOriginalPath(String originalPath) {
        this.originalPath = originalPath;
    }

    public String getOtherPath() {
        return otherPath;
    }

    public void setOtherPath(String otherPath) {
        this.otherPath = otherPath;
    }

    public String getOriginalElementId() {
        return originalElementId;
    }

    public void setOriginalElementId(String originalElementId) {
        this.originalElementId = originalElementId;
    }

    public static Request parseRequest(String... in) {

        if (in == null || in.length < 2) {
            throw new IllegalArgumentException("Request should contain at least two parameters: <input_origin_file_path> and <destination-path>. Optionally the third parameter may be <original_element_id>");
        }
        Request request = new Request(in[0], in[1]);

        if (in.length >= 3) {
            request.setOriginalElementId(in[2]);
        }

        return request;
    }
}
