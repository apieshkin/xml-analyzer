package com.agileengine;

import com.agileengine.model.Request;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

import static com.agileengine.JsoupCssSelectSnippet.*;

public class SmartXMLAnalyzer {

    public String findElementPath(Request request) {
        List<String> filters = createFilters(request.getOriginalPath(), request.getOriginalElementId());

        return getElementPath(
                findElementByQueries(filters,
                        new File(request.getOtherPath()))
                        .first());
    }

    private List<String> createFilters(String originalPath, String targetElementId) {
        Element element = findElementById(new File(originalPath), targetElementId);

        if(element==null){
            throw new NoSuchElementException(String.format("Original element id '%s' is not found",targetElementId));
        }

        return element.attributes().asList().stream()
                .map(attr -> String.format("a[%s=%s]", attr.getKey(), attr.getValue()))
                .collect(Collectors.toList());
    }

    private Elements findElementByQueries(List<String> stringifiedAttributesOpt, File resourcePath) {
        for (String query : stringifiedAttributesOpt) {
            Elements searchResults = findElementsByQuery(resourcePath, query);

            if (isEmptyResult(searchResults)) {
                continue;
            }
            for (String query2 : stringifiedAttributesOpt) {
                if (query2.equals(query)) {
                    continue;
                }
                Elements searchResults2 = searchResults.select(query2);

                if (!isEmptyResult(searchResults2))
                    return searchResults2;

            }
        }
        throw new NoSuchElementException("");
    }

    private boolean isEmptyResult(Elements searchResults) {
        return searchResults == null || searchResults.isEmpty();
    }
}
