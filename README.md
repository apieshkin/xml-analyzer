# Smart XML Analyzer

An application that generates xpath to the XML element of the target file, that has two similar element attributes of the source xml file.

### Prerequisites

 - Java 8+
 - Maven 3

### Installing

- Clone the repository

```
git clone https://apieshkin@bitbucket.org/apieshkin/xml-analyzer.git
```
- Build the application

```
cd xml-analyzer
mvn package
```
- Run the application

```
java -jar ./target/HtmlAnalyzer-1.0.1.jar <input_origin_file_path> <input_other_sample_file_path>
```
or
```
java -jar ./target/HtmlAnalyzer-1.0.1.jar <input_origin_file_path> <input_other_sample_file_path> <origin_element_id>
```

## Usage examples

HTML examples may be found [here](https://agileengine.bitbucket.io/keFivpUlPMtzhfAy/samples/pack/startbootstrap-sb-admin-2-examples.zip). Origin [HTML](https://agileengine.bitbucket.io/keFivpUlPMtzhfAy/samples/sample-0-origin.html).

Test run #1
```
java -jar ./target/HtmlAnalyzer-1.0.1.jar ~/test/sample-0-origin.html ~/test/sample-2-container-and-clone.html
```
Result #1
```
{#document[1]/html[1]/body[2]/div[1]/div[2]/div[3]/div[1]/div[1]/div[2]/div[1]/a[1]}
```
Test run #2
```
java -jar ./target/HtmlAnalyzer-1.0.1.jar ~/test/sample-0-origin.html ~/test/sample-2-container-and-clone.html make-everything-ok-button
```
Result #2
```
{#document[1]/html[1]/body[2]/div[1]/div[2]/div[3]/div[1]/div[1]/div[2]/div[1]/a[1]}
```
## Authors

* **Anton Pieshkin**